"use strict";

var gulp = require("gulp"),
    autoprefixer = require("gulp-autoprefixer"),
    cssbeautify = require("gulp-cssbeautify"),
    removeComments = require('gulp-strip-css-comments'),
    rename = require("gulp-rename"),
    sass = require("gulp-scss"),
    cssnano = require("gulp-cssnano"),
    rigger = require("gulp-rigger"),
    uglify = require("gulp-uglify"), //минимификатор в js
    watch = require("gulp-watch"),
    plumber = require("gulp-plumber"),
    imagemin = require("gulp-imagemin"),
    run = require("run-sequence"),
    rimraf = require("rimraf"),
    webserver = require("browser-sync"),
//max
    pug = require('gulp-pug'),
    pugPHPFilter = require('pug-php-filter'),
    concat = require('gulp-concat'),
    newer = require('gulp-newer'),
    jadePhp = require('gulp-jade-php');
//max


var homeFolder = "dev/",
    themeFolder = "web/wp-content/themes/spst/";

/* Paths to source/build/watch files
=========================*/

var path = {
    build: {
        pug: themeFolder,
        php: themeFolder,
        others: themeFolder,
        js: themeFolder+"js/",
        scss: themeFolder,
        css: themeFolder+"css/",
        img: themeFolder+"img/",
        fonts:  themeFolder+"fonts/"
    },
    src: {
        // html: "src/*.{htm,html}",
        pug: homeFolder+"pug/**/*.pug",
        php: homeFolder+"php/**/*.php",
        others: homeFolder+"others/**/*.*",
        js: homeFolder+"js/*.js",
        scss: homeFolder+"scss/style.scss",
        css: homeFolder+"scss/*.css",
        img: homeFolder+"img/**/*.*",
        fonts: homeFolder+"fonts/**/*.*"
    },
    watch: {
        // html: "src/**/*.{htm,html}",
        pug: homeFolder+"pug/**/*.pug",
        php: homeFolder+"php/**/*.php",
        js: homeFolder+"js/**/*.js",
        scss: homeFolder+"scss/**/*.scss",
        css: homeFolder+"scss/*.css",
        img: homeFolder+"img/**/*.*",
        fonts: homeFolder+"fonts/**/*.*"
    },
    clean: "./"+themeFolder
};



/* Webserver config
=========================*/

var config = {
    server: "build/",
    notify: false,
    open: true,
    ui: false
};



/* Tasks
=========================*/

gulp.task("webserver", function () {
    webserver(config);
});


// gulp.task("html:build", function () {
//     return gulp.src(path.src.html)
//         .pipe(plumber())
//         .pipe(rigger())
//         .pipe(gulp.dest(path.build.html))
//         .pipe(webserver.reload({stream: true}));
// });

//max
gulp.task('pug:build', function () {
    return gulp.src(path.src.pug)
        .pipe(plumber())
        .pipe(pug({
            pretty: "\t",
            filters: {
                php: pugPHPFilter
            }
        }))
        .pipe(rename(function (path) {
            path.extname = ".php"
        }))
        .pipe(gulp.dest(path.build.pug))
        .pipe(webserver.reload({stream: true}));
});
//max


gulp.task("scss:build", function () {
    gulp.src(path.src.scss)
        .pipe(plumber())
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ["last 5 versions"],
            cascade: true
        }))
        .pipe(removeComments())
        // .pipe(cssbeautify())
        // .pipe(gulp.dest(path.build.scss))
        .pipe(cssnano({
            zindex: false,
            discardComments: {
                removeAll: true
            }
        }))
        .pipe(gulp.dest(path.build.scss))
        .pipe(webserver.reload({stream: true}));
});


gulp.task("css:build", function () {
    gulp.src(path.src.css)
        .pipe(plumber())
        // .pipe(cssnano({
        //     zindex: false,
        //     discardComments: {
        //         removeAll: true
        //     }
        // }))
        .pipe(gulp.dest(path.build.css))
        .pipe(webserver.reload({stream: true}));
});



gulp.task("js:build", function () {
    gulp.src(path.src.js)
        .pipe(plumber())
        .pipe(rigger())
        .pipe(concat('script.js'))
        .pipe(gulp.dest(path.build.js))
        
        .pipe(uglify())
        .pipe(removeComments())
        .pipe(rename("script.min.js"))
        .pipe(gulp.dest(path.build.js))
        .pipe(webserver.reload({stream: true}));
});


gulp.task("fonts:build", function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
});


gulp.task("image:build", function () {
    gulp.src(path.src.img)
        .pipe(imagemin({
            optimizationLevel: 3,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img));
});


gulp.task("php:build", function() {
    gulp.src(path.src.php)
        .pipe(gulp.dest(path.build.php));
});

gulp.task("others:build", function() {
    gulp.src(path.src.others)
        .pipe(gulp.dest(path.build.others));
});



// Копируем php
// gulp.task('php:build', function() {
//     return gulp.src(path.src.php)
//         .pipe(newer(path.build.php))
//         .pipe(gulp.dest(path.build.php))
//         .pipe(reload({ stream: true }));
// });



// Копируем шрифты
// gulp.task('fonts', function() {
// return gulp.src(config.build.src.fonts)
//     .pipe(newer(config.build.dest.fonts))
//     .pipe(gulp.dest(config.build.dest.fonts))
//     .pipe(reload({ stream: true }));
// });






gulp.task("clean", function (cb) {
    rimraf(path.clean, cb);
});


gulp.task('build', function (cb) {
    run(
        "clean",
        // "html:build",
        "pug:build",
        "php:build",
        "others:build",
        "scss:build",
        "css:build",
        "js:build",
        "fonts:build",
        "image:build"
    , cb);
});


gulp.task("watch", function() {
    // watch([path.watch.html], function(event, cb) {
    //     gulp.start("html:build");
    // });
    watch([path.watch.pug], function(event, cb) {
        gulp.start("pug:build");
    });
    watch([path.watch.php], function(event, cb) {
        gulp.start("php:build");
    });
    watch([path.watch.scss], function(event, cb) {
        gulp.start("scss:build");
    });
    watch([path.watch.css], function(event, cb) {
        gulp.start("css:build");
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start("js:build");
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start("image:build");
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start("fonts:build");
    });
});


gulp.task("default", function (cb) {
   run(
       "clean",
       "build",
    //    "webserver",
       "watch"
   , cb);
});
