<?php /**
* Template Name: Booking Template
*
* @package SpSt
*
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lb
 */

get_header();
?>
<video class="bg-video" autoplay loop>
	<source src = "<?php echo get_template_directory_uri() . '/media/Contacts.mp4' ?>">
	</video>
	<div class="content-area">
		<main class="booking_page">
			<div class="booking">
				<h1 class="booking_title">
					<?php the_title(); ?>
				</h1>
				<div class="booking_content">					
					<div class="booking_country">
						<h2 class="booking_country_name">
							<span>
								<?php the_field('usa_title'); ?>
							</span>
							<span>
								<?php the_field('usa_agency_name'); ?>
							</span>
						</h2>
						<div class="booking_country_content" >
							<section class="booking_item">
								<h3 class="booking_name">
									<?php the_field('usa_contact_name1'); ?>
								</h3>
								<a class="booking_phone" href="tel:<?php the_field('usa_contact_phone1'); ?>" >
									<?php the_field('usa_contact_phone1'); ?>
								</a>
								<a class="booking_email" href="mailto:<?php the_field('usa_contact_email1'); ?>" >
									<?php the_field('usa_contact_email1'); ?>
								</a>
							</section><!-- booking_item -->
							<section class="booking_item">
								<h3 class="booking_name">
									<?php the_field('usa_contact_name2'); ?>
								</h3>
								<a class="booking_phone" href="tel:<?php the_field('usa_contact_phone2'); ?>" >
									<?php the_field('usa_contact_phone2'); ?>
								</a>
								<a class="booking_email" href="mailto:<?php the_field('usa_contact_email2'); ?>" >
									<?php the_field('usa_contact_email2'); ?>
								</a>
							</section><!-- booking_item -->
						</div><!-- booking_country_content -->
					</div><!-- booking_country -->
					<div class="booking_content_line">
						<div class="booking_country">
							<h2 class="booking_country_name">
								<?php the_field('eu_title'); ?>
							</h2>
							<div class="booking_country_content" >
								<section class="booking_item">
									<h3 class="booking_name">
										<?php the_field('eu_contact_name'); ?>
									</h3>
									<a class="booking_phone" href="tel:<?php the_field('eu_contact_phone'); ?>" >
										<?php the_field('eu_contact_phone'); ?>
									</a>
									<a class="booking_email" href="mailto:<?php the_field('eu_contact_email'); ?>" >
										<?php the_field('eu_contact_email'); ?>
									</a>
								</section><!-- booking_item -->
							</div><!-- booking_country_content -->
						</div><!-- booking_country -->
						<div class="booking_country">
							<h2 class="booking_country_name">
								<?php the_field('ua_title'); ?>
							</h2>
							<div class="booking_country_content" >
								<section class="booking_item">
									<h3 class="booking_name">
										<?php the_field('ua_contact_name'); ?>
									</h3>
									<a class="booking_phone" href="tel:<?php the_field('ua_contact_phone'); ?>" >
										<?php the_field('ua_contact_phone'); ?>
									</a>
									<a class="booking_email" href="mailto:<?php the_field('ua_contact_email'); ?>" >
										<?php the_field('ua_contact_email'); ?>
									</a>
								</section><!-- booking_item -->
							</div><!-- booking_country_content -->
						</div><!-- booking_country -->
					</div>
				</div><!-- booking_content -->
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

	<?php
	get_footer();
