<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lb
 */

?>

<div class="about_content">
	<div class="entry-content">
		<?php
		the_content();
		?>
	</div><!-- .entry-content -->
	<div class="about_awards">
		<h2 class="about_awards_title" >Awards</h2>
		<?php include(locate_template( 'awards-loop/loop-index.php' ));?>
	</div>
	<div class="about_clients">
		<h2 class="about_clients_title" >Partners & Clients</h2>
		<?php include(locate_template( 'clients-loop/loop-index.php' ));?>
	</div>
</div>

