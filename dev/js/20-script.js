var windowWidth = jQuery(window).width();

jQuery(document).ready(function ($) {

	// -------------------------------------------------------------------------------------
	// -------------------------------Front page video--------------------------------------
	function frontPageVideo(){
		var video = $(".bg-video.main-page_video");
		if(video.length){
			function changeVideo(view){
				var src = $(".bg-video.main-page_video source");
				if(view == "mobile"){
					src.attr("src", "/wp-content/themes/spst/media/LB_mobile.mp4");
					video[0].load();
					// plreload();
				}
				else if(view == "desktop"){
					src.attr("src", "/wp-content/themes/spst/media/LB_desktop.mp4");
					video[0].load();
					// plreload();
				}
			}
			function viewDependency(){
				var width = $(window).width();
				if(width < 992){
					changeVideo("mobile");
					// plreload();
				}
				else if(width >= 992){
					changeVideo("desktop");
					// plreload();
				}
			}
			viewDependency();
			// $(window).on('resize', function(){
			// 	viewDependency();
			// })
		}
		return plreload();
	}
	frontPageVideo();
	// -------------------------------Front page video--------------------------------------
	// -------------------------------------------------------------------------------------

	// -------------------------------------------------------------------------------------
	// ----------------------------------Contact us-----------------------------------------
	if(jQuery("textarea").length){
		var textarea = document.querySelector('textarea');
		textarea.addEventListener('keydown', autosize);
		function autosize(){
		var el = this;
		setTimeout(function(){
			el.style.cssText = 'height:auto; padding:0';
			el.style.cssText = 'height:' + el.scrollHeight + 'px';
		},0);
		}
	}
	// ----------------------------------Contact us-----------------------------------------
	// -------------------------------------------------------------------------------------

	function LockElement(element){
		this.lock = function(){
			element.css('overflow-y', 'hidden');
		}
		this.unlock = function(){
			element.css('overflow-y', 'scroll');
		}
	}

	var body = new LockElement($('body'));
	var menu = new LockElement($('.site-header'));

	function slideDownElement(elem, extraElem, extraElem2, extraElem3, extraElem4, extraElem5, extraElem6, speed) {
		if ($(elem).data("obey") === undefined) { $(elem).data("obey", true); }
		if (elem.hasClass('active') || !elem.data("obey")) {
			return;
		}
		elem.data("obey", false);
		elem.addClass("active");
		body.lock();
		menu.unlock();
		if (extraElem != undefined) { extraElem.addClass("active"); }
		if (extraElem2 != undefined) { extraElem2.addClass("active"); }
		if (extraElem3 != undefined) { extraElem3.addClass("active"); }
		if (extraElem4 != undefined) { extraElem4.addClass("active"); }
		if (extraElem5 != undefined) { extraElem5.addClass("active"); }
		if (extraElem5 != undefined) { extraElem6.addClass("active"); }
		if (speed == undefined) { speed = 400; }
		elem.fadeIn(speed, function () {
			elem.data("obey", true);
		});
	}
	function slideUpElement(elem, extraElem, extraElem2, extraElem3, extraElem4, extraElem5, extraElem6, speed) {
		if (elem.data("obey") === undefined) { elem.data("obey", true); }
		if (!elem.hasClass('active') || !elem.data("obey")) {
			return;
		}
		elem.data("obey", false);
		elem.removeClass("active");
		body.unlock();
		menu.lock();
		if (extraElem != undefined) { extraElem.removeClass("active"); }
		if (extraElem2 != undefined) { extraElem2.removeClass("active"); }
		if (extraElem3 != undefined) { extraElem3.removeClass("active"); }
		if (extraElem4 != undefined) { extraElem4.removeClass("active"); }
		if (extraElem5 != undefined) { extraElem5.removeClass("active"); }
		if (extraElem5 != undefined) { extraElem6.removeClass("active"); }
		if (speed == undefined) { speed = 400; }
		elem.slideUp(speed, function () {
			elem.data("obey", true);
		});
	}


	if(windowWidth < 991){
		$('.menu-item a').click(function(){
			$('.menu-item a').removeClass('active');
			$(this).addClass('active');
		});
	}	


	$('.slider_video').slick({
		dots: true,
		arrows: false,
		fade: true,
		cssEase: 'linear',
	});


	$('.main_slider').slick({
		dots: true,
		arrows: false,		
		responsive: [
		{
			breakpoint: 768,
			settings: {
				dots: false,
				autoplay: true,
			},
		},
		],
	});


	if(windowWidth < 768){
		$('.slide-count').addClass('active');
		$('.tourdates_list').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
			var i = (currentSlide ? currentSlide : 0) + 1;
			$('.slide-count').text(i + '/' + slick.slideCount);
		});
	}else{
		$('.slide-count').removeClass('active');
	}

	$('.tourdates_list').slick({		
		swipe: true,
		swipeToSlide: true,
		vertical: true,
		slidesToShow: 5,
		dots: false,
		arrows: false,
		infinite: true,
		verticalSwiping: true,
		responsive: [
		{
			breakpoint: 767,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				vertical: false,
				verticalSwiping: false,
				autoplay: true,
			}
		},
		],
	});

	$('.tourdates_list, .main_slider, .slider_video').on('wheel', (function (e) {
		e.preventDefault();
		if (e.originalEvent.deltaY < 0) {
			$(this).slick('slickPrev');
		} else {
			$(this).slick('slickNext');
		}
	}));

	$('.slick-dots button').text('');

	// $('.contact_messege').attr('rows', 3);


	// $('.contact_messege').attr('rows', 1);


	var initPlayer = function (element) {
		var player = element.querySelector('.video-iframe');
		var button = element.querySelector('.video-play');
		var ytplayer = new YT.Player(player, {
			playerVars: {
				'autoplay': 0,
				'modestbranding': 1,
				'controls': 0,
				'rel': 0,
				'iv_load_policy': 3,
				'rel': 0,
				'showinfo': 0,
			},
			videoId: element.dataset.id
		});

		button.addEventListener('click', function () {
			switch (ytplayer.getPlayerState()) {
				case 1:
				ytplayer.pauseVideo();
				$('.video-play').addClass('active');
				break;
				default:
				ytplayer.playVideo();
				$('.video-play').removeClass('active');
				break;
			}
		})

		$('.slider_video').on('afterChange', function (event, slick, direction) {
			if (ytplayer.getPlayerState()) {
				ytplayer.stopVideo();
				$('.video-play').addClass('active');
			}
		});
	}


	window.onYouTubePlayerAPIReady = function () {
		var container = document.querySelectorAll('.video-container');
		for (var i = 0; i < container.length; i++) {
			initPlayer(container[i]);
		}
	}

	//  header menu

	$('.gamburger-button').on('click', function () {
		slideDownElement($('.menu-main-menu-container'), $('.main-dots'), $('.gamburger-button'), $('.gamburger-button_close'), $('.social_links__phone'), $('.header__top-block'), $('.site-header'));
		slideUpElement($('.menu-main-menu-container'), $('.main-dots'), $('.gamburger-button'), $('.gamburger-button_close'), $('.social_links__phone'), $('.header__top-block'), $('.site-header'));
	});


	//  end header menu	


	///////////*Shopify*//////////////////

 

	(function () {
		var scriptURL = 'https://sdks.shopifycdn.com/buy-button/latest/buy-button-storefront.min.js';
		if (window.ShopifyBuy) {
			if (window.ShopifyBuy.UI) {
				ShopifyBuyInit();
			} else {
				loadScript();
			}
		} else {
			loadScript();
		}

		function loadScript() {
			var script = document.createElement('script');
			script.async = true;
			script.src = scriptURL;
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(script);
			script.onload = ShopifyBuyInit;
		}

		function ShopifyBuyInit() {
			var client = ShopifyBuy.buildClient({
				domain: 'lightbooking.myshopify.com',
				storefrontAccessToken: 'e467c50bf5a3775ae23d4e4a3ef857d5',
			});

			ShopifyBuy.UI.onReady(client).then(function (ui) {
				ui.createComponent('collection', {
					id: 83787612253,
					node: document.getElementById('collection-component-31035381d1f'),
					moneyFormat: '%E2%82%B4%7B%7Bamount%7D%7D',
				  options: {
						"product": {
						
						"iframe":false,
							"buttonDestination": "modal",
							"variantId": "all",
							"contents": {
								"imgWithCarousel": false,
								"variantTitle": false,
								"options": false,
								"description": false,
								"buttonWithQuantity": false,
								"quantity": false,
								"title": false
							},
							"text": {
								"button": "DETAIL" 
							},
							"styles": {
								"product": {
									"display":"flex",
									"@media (min-width: 601px)": {
										"max-width": "calc(33.33333% - 30px)",
										"margin-left": "30px",
										"margin-bottom": "50px",
										"width": "calc(33.33333% - 30px)"
									},
									"imgWrapper": { 
										"position": "relative",
										"height": "0",
										"padding-top": "calc(75% + 15px)"
									},
									"img": {
										"height": "calc(100% - 15px)",
										"position": "absolute",
										"left": "0",
										"right": "0",
										"top": "0"
									}
								}
							}
						},
						'modal':{
						"iframe":false,
						"description":true
						},
						"cart": {
						
							"contents": {
								"button": true
							},
							"styles": {
								"footer": {
									"background-color": "#ffffff"
								}
							}
						},
						"toggle":{
							"iframe":false,
							events: {
								'beforeInit':function (component) {
									// if(jQuery('.shopify-buy-frame--toggle').hasClass('is-active')){
									// 	jQuery('.shop_title').addClass("toggle");
									// }else{
									// 	jQuery('.shop_title').removeClass("toggle");
									// }
								
								},
								'afterInit': function (component) {
									document.getElementById('cart-toggle').append(component.node);
								 
								}
							}
			
						},

						"modalProduct": {
						"iframe":false,
							"contents": {
								"img": false,
								"imgWithCarousel": true,
								"variantTitle": false,
								"buttonWithQuantity": true,
								"button": false,
								"quantity": false
							},
							"styles": {
								"product": {
									"@media (min-width: 601px)": {
										"max-width": "100%",
										"margin-left": "0px",
										"margin-bottom": "0px"
									}
								}
							}
						},
						"cart":{
							"iframe":false,
					 
						},
						
						"productSet": {
							events: {
								'beforeInit': function (component) {
										jQuery(".site-footer").hide();
									},
								'afterInit': function (component) {
										jQuery("#preloader_shop").hide();
										jQuery(".site-footer").css('display','flex');
									}
								},
						"iframe":false,
							"styles": {
								"display":'flex',
								"products": {
									"display":'flex',
									"@media (min-width: 601px)": {
										"margin-left": "-30px"
									}
								}
							}
						}
					}
				});
			});
		}
	})();

	///////////* END Shopify*//////////////////

});

function plreload(){

	
	jQuery(document).ready(function () {

		if(jQuery("video").is(".main-page_video") && windowWidth < 992){
			// console.log("IF");
			// var videoUploadTimer = setTimeout(function(){
			// 	jQuery(".preloader").delay(100).fadeOut().remove();
			// 	// jQuery('.main-page_video').remove();
			// 	// jQuery('.main_page').addClass('bg-active');
			// }, 5000);
			// jQuery('.main-page_video').on('canplay', function(){
				// console.log("Can Play [x_x]");
				// clearTimeout(videoUploadTimer);
				jQuery(".preloader").delay(100).fadeOut().remove();
				jQuery("body").removeClass('blocked');

				// console.log("KILL PRELOADER");	
			// });
		}else{
			// console.log("ELSE");
			jQuery(".preloader").delay(100).fadeOut().remove();	
			jQuery("body").removeClass('blocked');
		}


	});
}
 
 
 