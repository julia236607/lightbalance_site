<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1">
		<link rel="profile" href="https://gmpg.org/xfn/11"><?php wp_head();
 ?>
	</head>
	<body <?php body_class('blocked'); ?>><?php get_header();
dynamic_sidebar( 'header' ); ?>
		<h1 class="shop_title"><?php the_title();  ?></h1>
		<main class="shop_page">
			<div id="preloader_shop">
				<div id="loader"></div>
			</div>
			<div id="cart-toggle"></div>
			<div id="collection-component-31035381d1f"></div>
		</main><?php get_footer();
dynamic_sidebar( 'footer' );
 ?><?php wp_footer(); ?>
	</body>
</html>