
<div class="preloader">  <img class="loader" src="<?php echo get_template_directory_uri() . '/media/loader.gif' ?>"/></div>
<div class="site-header" id="masthead">
	<div class="header__top-block"><a class="header-logo_link" href="<?php echo get_home_url(); ?>"><img class="header-logo" alt="LB" src="<?php echo get_template_directory_uri() . '/media/Logo.svg' ?>"/></a>
		<div class="gamburger-button">
			<div class="gamburger-button__line"></div>
			<div class="gamburger-button__line"></div>
			<div class="gamburger-button__line"></div>
		</div>
		<div class="gamburger-button gamburger-button_close">
			<div class="gamburger-button__line"></div>
			<div class="gamburger-button__line"></div>
		</div>
	</div><?php wp_nav_menu( array('menu' => 'Main Menu' )); ?>
	<div class="social_links social_links__phone"><a class="social_links_item" href="<?php the_field('facebook_link', 167); ?>" target="_blank"><img alt="alt" src="<?php echo get_template_directory_uri() . '/media/fb-red.svg' ?>"/></a><a class="social_links_item" href="<?php the_field('instagram_link', 167); ?>" target="_blank"><img alt="alt" src="<?php echo get_template_directory_uri() . '/media/insta-red.svg' ?>"/></a><a class="social_links_item" href="<?php the_field('youtube_link', 167); ?>" target="_blank"><img alt="alt" src="<?php echo get_template_directory_uri() . '/media/youtube-red.svg' ?>"/></a><a class="social_links_item" href="<?php the_field('twitter_link', 167); ?>" target="_blank"><img alt="alt" src="<?php echo get_template_directory_uri() . '/media/Twitter_Logo.svg' ?>"/></a></div>
</div>
<div class="dots-wrapper">  
	<div class="main-dots"></div>
	<div class="main-dots booking-dots-second"></div>
</div>