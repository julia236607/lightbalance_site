<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1">
		<link rel="profile" href="https://gmpg.org/xfn/11"><?php wp_head();
 ?>
	</head>
	<body <?php body_class('blocked'); ?>><?php get_header();
dynamic_sidebar( 'header' ); ?>
		<video class="bg-video tourdates-video" autoplay loop muted>
			<source src="<?php echo get_template_directory_uri() . '/media/Tourdates_2560.4078c8a4.mp4' ?>">
		</video>
		<div class="content-area">
			<main class="tourdates_page">
				<div class="tourdates"><?php $my_postid = 107;
$content_post = get_post($my_postid);
$content = $content_post->post_content;
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]&gt;', $content);
echo $content;
global $query_string;
query_posts( $query_string.'&posts_per_page=90'); ?><span class="slide-count"></span>
					<div class="tourdates_list"><?php while ( have_posts() ) {      
  the_post();
  get_template_part( 'template-parts/content', get_post_type() );
}
wp_reset_query(); ?>
					</div>
				</div>
			</main>
		</div><?php get_footer();
dynamic_sidebar( 'footer' );
 ?><?php wp_footer(); ?>
	</body>
</html>