<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1">
		<link rel="profile" href="https://gmpg.org/xfn/11"><?php wp_head();
 ?>
	</head>
	<body <?php body_class('blocked'); ?>><?php get_header();
dynamic_sidebar( 'header' ); ?>
		<video class="bg-video contact-us-video" autoplay loop muted>
			<source src="<?php echo get_template_directory_uri() . '/media/Dracula.mp4' ?>">
		</video>
		<div class="content-area contact-us-area">
			<main class="booking_page">
				<div class="contact-us">
					<h1 class="booking_title">Feel free to drop us a line</h1>
					<div class="contact_form"><?php echo do_shortcode('[contact-form-7 id="166" title="ContactUs"]'); ?></div>
				</div>
			</main>
		</div><?php get_footer();
dynamic_sidebar( 'footer' );
 ?><?php wp_footer(); ?>
	</body>
</html>