
<article id="post-<?php the_ID(); ?>" onclick="playVideo('<?php echo get_field('video_url');; ?>');">
	<div class="background-image"><?php $screenshoot = get_post_meta($post->ID, 'image', true);
echo wp_get_attachment_image($screenshoot, 'original'); ?>
	</div>
	<div class="duration-text">
		Duration :<?php echo get_field('duration'); ?></div>
	<header class="entry-header"><?php the_title( '<h1 class="entry-title">', '</h1>' ); ?></header>
</article>