
<article id="post-<?php the_ID(); ?>">
	<div class="event_item">
		<div class="event_content">
			<div class="date-text"><?php echo get_field('date'); ?></div>
			<header class="event_header"><?php the_title( '<h2 class="event_title">', '</h2>' ); ?></header>
			<div class="event_desc-txt"><?php echo get_the_content(); ?></div>
		</div>
		<button class="event_btn btn_anim" onclick="window.open('<?php echo get_field('booking_url');; ?>', '_blank')"><?php echo get_field('booking_text'); ?></button>
	</div>
</article>