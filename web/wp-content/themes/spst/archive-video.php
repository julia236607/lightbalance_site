<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1">
		<link rel="profile" href="https://gmpg.org/xfn/11"><?php wp_head();
 ?>
	</head>
	<body <?php body_class('blocked'); ?>><?php get_header();
dynamic_sidebar( 'header' ); ?>
		<div class="slider_video"><?php global $post;
$args = array( 'posts_per_page' => -1, 'post_type' => 'video', 'post_status' => 'publish');
$myposts = get_posts( $args );
foreach ( $myposts as $post ) {
  setup_postdata( $post ); ?>
			<div class="video-container" data-id="<?php the_field('video_id') ?>">
				<div class="video-iframe"></div>
				<div class="video-play active"><img alt src="<?php the_post_thumbnail_url(); ?>">
					<div class="video-play_btn btn_anim">
						<div class="video-play_btn_triangle"></div>
					</div>
					<div class="video-info"><span class="video-info_duration">
							Duration: <?php the_field('video_duration'); ?></span>
						<h2 class="video-info_title"><?php the_title(); ?></h2>
					</div>
				</div>
			</div><?php }
wp_reset_postdata();
     ?>
		</div><?php get_footer();
dynamic_sidebar( 'footer' );
 ?><?php wp_footer(); ?>
	</body>
</html>