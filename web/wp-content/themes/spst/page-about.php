<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1">
		<link rel="profile" href="https://gmpg.org/xfn/11"><?php wp_head();
 ?>
	</head>
	<body <?php body_class('blocked'); ?>><?php get_header();
dynamic_sidebar( 'header' ); ?>
		<video class="bg-video about-video" autoplay loop muted>
			<source src="<?php echo get_template_directory_uri() . '/media/About.mp4' ?>">
		</video>
		<div class="content-area about-page">
			<main class="about_page">
				<div class="about"><?php while ( have_posts() ) :
the_post();
get_template_part( 'template-parts/content', 'about' );
endwhile; // End of the loop. ?>
				</div>
			</main>
		</div><?php get_footer();
dynamic_sidebar( 'footer' );
 ?><?php wp_footer(); ?>
	</body>
</html>