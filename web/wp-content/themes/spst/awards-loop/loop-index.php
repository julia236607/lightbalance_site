
<ul class="award-items"><?php global $post;
$args = array( 'posts_per_page' => -1, 'post_type' => 'award', 'post_status' => 'publish');
$myposts = get_posts( $args );
foreach ( $myposts as $post ) {
  setup_postdata( $post );
  include(locate_template( 'awards-loop/loop-item.php' ));
}
wp_reset_postdata(); ?>
</ul>