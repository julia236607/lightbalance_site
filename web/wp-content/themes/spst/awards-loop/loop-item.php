
<li class="award-item">
	<div class="award-logo"><img alt="alt" src="<?php echo get_template_directory_uri() . '/media/olive-branches-award-symbol.svg' ?>"/></div>
	<h3 class="award-year"><?php echo get_field('year', $post->ID); ?></h3>
	<h2 class="award-title"><?php echo get_the_title($post->ID); ?></h2>
	<div class="award-place"><?php echo get_the_content($post->ID); ?></div>
</li>