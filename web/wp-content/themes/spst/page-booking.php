<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1">
		<link rel="profile" href="https://gmpg.org/xfn/11"><?php wp_head();
 ?>
	</head>
	<body <?php body_class('blocked'); ?>><?php get_header();
dynamic_sidebar( 'header' ); ?>
		<video class="bg-video booking-video" autoplay loop muted>
			<source src="<?php echo get_template_directory_uri() . '/media/Contacts.mp4' ?>">
		</video>
		<div class="content-area booking-area">
			<main class="booking_page">
				<div class="booking">
					<h1 class="booking_title"><?php the_title(); ?></h1>
					<div class="booking_content">
						<div class="booking_country">
							<h2 class="booking_country_name"><span><?php the_field('usa_title');  ?></span><span><?php the_field('usa_agency_name'); ?></span></h2>
							<div class="booking_country_content">
								<section class="booking_item">
									<h3 class="booking_name"><?php the_field('usa_contact_name1');  ?></h3><a class="booking_phone" href="tel:<?php the_field('usa_contact_phone1'); ?>"><?php the_field('usa_contact_phone1');  ?></a><a class="booking_email" href="mailto:<?php the_field('usa_contact_email1'); ?>"><?php the_field('usa_contact_email1');  ?></a>
								</section>
								<section class="booking_item">
									<h3 class="booking_name"><?php the_field('usa_contact_name2'); ?></h3><a class="booking_phone" href="tel:<?php the_field('usa_contact_phone2'); ?>"><?php the_field('usa_contact_phone2');  ?></a><a class="booking_email" href="mailto:<?php the_field('usa_contact_email2'); ?>"><?php the_field('usa_contact_email2');  ?></a>
								</section>
							</div>
							<h2 class="booking_country_name"><span><?php the_field('usa_management _title'); ?></span></h2>
							<div class="booking_country_content">
								<section class="booking_item">
									<h3 class="booking_name"><?php the_field('usa_contact_name3');  ?></h3><a class="booking_phone" href="tel:<?php the_field('usa_contact_phone3'); ?>"><?php the_field('usa_contact_phone3');  ?></a><a class="booking_email" href="mailto:<?php the_field('usa_contact_email3'); ?>"><?php the_field('usa_contact_email3');  ?></a>
								</section>
								<section class="booking_item">
									<h3 class="booking_name"><?php the_field('usa_contact_name4'); ?></h3><a class="booking_phone" href="tel:<?php the_field('usa_contact_phone4'); ?>"><?php the_field('usa_contact_phone4');  ?></a><a class="booking_email" href="mailto:<?php the_field('usa_contact_email4'); ?>"><?php the_field('usa_contact_email4');   ?></a>
								</section>
							</div>
						</div>
						<div class="booking_content_line">
							<div class="booking_country">
								<h2 class="booking_country_name"><?php the_field('eu_title');  ?></h2>
								<div class="booking_country_content">
									<section class="booking_item">
										<h3 class="booking_name"><?php the_field('eu_contact_name');  ?></h3><a class="booking_phone" href="tel:<?php the_field('eu_contact_phone'); ?>"><?php the_field('eu_contact_phone');                                 ?></a><a class="booking_email" href="mailto:<?php the_field('eu_contact_email'); ?>"><?php the_field('eu_contact_email');                                 ?></a>
									</section>
								</div>
							</div>
							<div class="booking_country">
								<h2 class="booking_country_name"><?php the_field('ua_title'); ?></h2>
								<div class="booking_country_content">
									<section class="booking_item">
										<h3 class="booking_name"><?php the_field('ua_contact_name'); ?></h3><a class="booking_phone" href="tel:<?php the_field('ua_contact_phone'); ?>"><?php the_field('ua_contact_phone');                                 ?></a><a class="booking_email" href="mailto:<?php the_field('ua_contact_email'); ?>"><?php the_field('ua_contact_email'); ?></a>
									</section>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
		</div><?php get_footer();
dynamic_sidebar( 'footer' );
 ?><?php wp_footer(); ?>
	</body>
</html>