<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1">
		<link rel="profile" href="https://gmpg.org/xfn/11"><?php wp_head();
 ?>
	</head>
	<body <?php body_class('blocked'); ?>><?php get_header();
dynamic_sidebar( 'header' ); ?>
		<video class="bg-video main-page_video" autoplay muted loop playsinline poster="<?php echo get_field('video_poster') ?>">
			<source type="video/mp4">
		</video>
		<div class="content-area">
			<main class="main_page">
				<div class="main_slider"><?php   global $post;
  $args = array( 'posts_per_page' => -1, 'post_type' => 'slide', 'post_status' => 'publish');
  $myposts = get_posts( $args );
  foreach ( $myposts as $post ) {
setup_postdata( $post ); ?>
					<div class="main_slider_item">
						<p class="main_slider_item_txt"><?php the_title(); ?></p>
					</div><?php   }   
wp_reset_postdata(); ?>
				</div>
			</main>
		</div><?php get_footer();
dynamic_sidebar( 'footer' );
 ?><?php wp_footer(); ?>
	</body>
</html>