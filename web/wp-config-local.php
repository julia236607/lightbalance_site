<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'c7_lb');

/** MySQL database username */
define('DB_USER', 'c7_lb');

/** MySQL database password */
define('DB_PASSWORD', 'jbiUt2XU8@');

/** MySQL hostname */
define('DB_HOST', 'lb.dev.splinestudio.com');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_HOME','http://lightbalance.localhost:8080');
define('WP_SITEURL','http://lightbalance.localhost:8080');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&v%8hP<=N+dN&P%.]6TS<pqQz0-yTQ)P5?dzDlJRUzK]phLX`1q.Kgfj#yMOV6?^');
define('SECURE_AUTH_KEY',  '%X{mNs/@=_<d8=6[Bmv<@9OCS[ln1(=GRvrwU%/;!xb@]u82T8-W: a.:o^_Rbny');
define('LOGGED_IN_KEY',    '#V(_~a[T`*5jXy0iE#E),O@&#;$hJpkFh!Z`P3 oTAk!U(u$f&lI;)%iG;5.f7E_');
define('NONCE_KEY',        '-a1Di`lCM?*ho}dBiFMtySLcJ|IZ;$vbBAsI*.zKADIR2 qchv|!9+JbE>V~&`LJ');
define('AUTH_SALT',        '%Fub 630tg?w&gzMx{VV0N(Vys~!y<? y2WSSw`_-tI8QqR]QvE$2D.i3T}G?v5-');
define('SECURE_AUTH_SALT', ')G^Yqby4w 2BN%4L`N#*qU(4[{~44~OMy@f|Y~s=XAfUJ:J/I>BzGee-auy?.V1.');
define('LOGGED_IN_SALT',   ')GR]mk_t)fjhj^!4$j_[y8JCC4e[U@x~*HfB$h!m*1/IHoq4qd [Y$L&Hdqd4Kf$');
define('NONCE_SALT',       'e0JA?frS|aasA.n2d GVcC>Xy0ygUR]U{{].m::,PIK8r[UxutnzqrEua,E#Ol*}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
